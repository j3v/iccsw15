\documentclass[a4paper,UKenglish]{oasics}

\usepackage{microtype}
\usepackage{colortbl}

\bibliographystyle{plain}

% Author macros::begin %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\definecolor{Gray}{gray}{0.90}

\title{Language Run-Time Systems: An Overview}
%\titlerunning{Language Runtime Systems} %optional, in case that the title is too long; the running title should fit into the top page column

\author[]{Evgenij Belikov}
%\author[2]{Joan R. Access}
\affil[]{Heriot-Watt University, School of Mathematical and Computer Sciences \\
  Riccarton, EH14 4AS, Edinburgh, Scotland, UK\\
\texttt{eb120@hw.ac.uk}}
%\affil[2]{Department of Informatics, Dummy College\\
%  Address, Country\\
%  \texttt{access@dummycollege.org}}
\authorrunning{E. Belikov}%mandatory. First: Use abbreviated first/middle names. 
%Second (only in severe cases): Use first author plus 'et. al.'

\Copyright{Evgenij Belikov} %mandatory. OASIcs license is "CC-BY";  http://creativecommons.org/licenses/by/3.0/

\subjclass{A.1 Introductory and Survey, D.3.4 Run-Time Environments} 

\keywords{Run-Time Systems, Virtual Machines, Adaptive Policy Control} % mandatory: Please provide 1-5 keywords
% Author macros::end %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%Editor-only macros:: begin (do not touch as author)%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\serieslogo{}%please provide filename (without suffix)
\volumeinfo%(easychair interface)
  {XX YY and ZZ ZZ} % editors
  {2}% number of editors: 1, 2, ....
  {Imperial College Computing Student Workshop (ICCSW’15)} % event
  {1}% volume
  {1}% issue
  {1}% starting page number
\EventShortName{}
\DOI{10.4230/OASIcs.ICCSW.2015.1}% to be completed by the volume editor 
% Editor-only macros::end %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{document}
\maketitle

\begin{abstract}
The proliferation of high-level programming languages with advanced language features and the need for portability 
across increasingly heterogeneous and hierarchical architectures require a sophisticated run-time system 
to manage program execution and available resources. Additional benefits include isolated execution of untrusted code 
and the potential for dynamic optimisation, among others. 
This paper provides a high-level overview of language run-time systems with a focus on execution models, support for concurrency 
and parallelism, memory management, and communication, whilst briefly mentioning synchronisation, monitoring, and adaptive policy control. 
Two alternative approaches to run-time system design are presented and several challenges for future research are outlined. 
References to both seminal and recent work are provided.
\end{abstract} 

\vspace{-0.1cm}
\section{Introduction}

Programming languages have evolved from assembly languages, where programmers have to specify 
a sequence of low-level instructions for a specific target platform, towards higher levels of 
expressiveness and abstraction. Imperative languages enabled \emph{structured programming} by 
simplifying the expression of nested conditionals, iteration, and by allowing the grouping of 
instructions to named procedures. More recently, object-oriented and declarative languages 
with  sophisticated type systems and support for generics, polymorphism, concurrency, and parallelism, 
among other advanced features, further improved \emph{portability} and \emph{productivity}.

It is widely known that given a set of $M$ programming languages and $N$ target platforms, using a \emph{platform-independent intermediate representation}, which abstracts over the instruction set of the target architecture, reduces the number of necessary translation components from $M * N$ to $M + N$ facilitating the implementation of and interoperability between programming languages across a wide range of diverse hardware architectures~\cite{richards1971portability,nori1974pascal}. 

As the advanced language features, such as automatic memory management, generics and reflection,
require support beyond the static capabilities of a compiler, a \emph{run-time system} (RTS, also referred to as \emph{high-level language virtual machine} -- a process-oriented sub-class of virtual machines~\cite{mccarthy1978history,deutsch1984efficient,holzle1994third,smith2005architecture}) was introduced to dynamically manage program execution whilst maintaining high performance, which program interpreters sacrifice in most cases~\cite{romer1996structure}.
The added flexibility of delayed specialisation and additional context information available at run time allow for dynamic optimisation for many applications with no hard real-time requirements, e.g.\ by detecting program hot spots and recompiling relevant program fragments just-in-time (JIT)~\cite{aycock2003brief,arnold2005survey}. Moreover, recent trends towards increasingly hierarchical and heterogeneous parallel architectures~\cite{OLGH07,BDHH10} and the promise of Big Data Analysis~\cite{mayer2013big} have reinvigorated the interest in language support for concurrency, parallelism, and Distributed Computing, based on advanced compiler and RTS-level functionalities~\cite{HW-MACS-TR-0103}.

This paper provides an overview of a set of representative
language run-time systems including both established systems and research prototypes, presents the key components of a generic RTS and discusses associated policies as well as two more radical alternative RTS designs. Finally, challenges and opportunities for future research are outlined. While the discussion remains rather brief and high-level, references to technical literature are provided. 


\section{The Architecture of a Generic Language Run-Time System}

Here, a \emph{run-time system} is defined as \emph{a software component responsible for dynamic management of program execution and of the resources granted by the OS}. A RTS is distinct from and embedded into a more broad \emph{run-time environment}, which is defined to include the OS that is responsible for overall resource management, and RTS instances managing other running applications unaware of each other's existence and resource demands. In other words, an RTS mediates interaction between the application and the OS whilst managing language-specific components in user space to avoid some context switching overhead~\cite{silberschatz1998operating}.

\subsection{Run-Time System in the Software Life Cycle}

The term \emph{software life cycle} describes the stages of the software development process and their relations.
Here the focus is on the compilation and execution stages.
The RTS is primarily used during the application run time, consisting of the startup, initialisation, execution and termination phases, in addition to potential dynamic linking, loading and JIT-compilation as well as ahead-of-time (AOT) compilation.

\begin{figure*}[ht!]
  \centerline{
    \includegraphics[scale=0.60]{rts-life-cycle.jpg}
  }
  \caption{RTS in the Software Life Cycle}
  \label{fig:lifecycle}
\end{figure*}

Figure~\ref{fig:lifecycle} illustrates the possible roles of the RTS in the software life cycle (excluding design, construction, and maintenance). For instance, C's minimalistic RTS (\texttt{crts0}) is added by the linker/loader and initialises the execution environment (e.g.\ the stack, stack/heap pointers, registers, signal handlers), transfers control to the program by calling the \texttt{main} function, and eventually passes on \texttt{main}'s return value to the run-time environment (e.g.\ the shell). However, it is platform-specific and implemented in assembler so that recompilation is required on different architectures, thus the \emph{abstract machine} functionality is not provided.

More sophisticated RTSes are either implemented as a separate library and linked to the application object code to create an executable program (e.g.\ GUM RTS for Glasgow parallel Haskell~\cite{THMP96}) or are themselves applications which receive program code as an input (e.g. Java Virtual Machine (JVM)~\cite{lindholm2014java}, and .NET Common Language Runtime (CLR)~\cite{meijer2001technical}). The input is usually in an \emph{intermediate representation form} (IR, virtual instruction set) that decouples symbolic programming language representation from the native instruction set of the target architecture, which is then either interpreted or JIT-compiled to native code. Ahead-of-time compilation can be used to compile IR to native code to avoid initially running the interpreted IR code until the JIT phase is complete to reduce start-up time.

\subsection{Run-Time System Structure and Function}
We focus on the general components corresponding to key RTS functions and associated \emph{policies}, i.e.\ \emph{sets of rules that define choices in the behaviour of the system}. We omit the discussion of RTSes for embedded platforms as
strict real-time requirements, often associated with embedded applications, preclude the use of an RTS. The main function of
the RTS is the management of program execution on the implemented abstract machine based on a specific execution model. 
The policies include interaction with the OS, memory management, thread and task management, communication, and monitoring, among others.

\begin{figure*}[ht!]
  \centerline{
    \includegraphics[scale=0.60]{rts-layers.jpg}
  }
  \caption{RTS Components in the Software Stack}
  \label{fig:layers}
\end{figure*}

\vspace{-0.8cm}
\subparagraph*{Execution Models} A common model is that of a \emph{stack machine} corresponding closely to the {Von-Neumann} architecture~\cite{hennessy2011computer}, where a stack is used to store function arguments and intermediate results returned from function calls. This sequential model is often employed in RTSes for imperative and object-oriented languages along with \emph{register machines}, where a stack is replaced by a set of virtual registers, which are mapped to the machine's registers. 

Another prominent model is \emph{graph reduction} commonly used with functional languages based on the lambda calculus~\cite{hughes1989functional,barendregt1984lambda}. This model is suitable for parallelism as parts of the graph can be reduced independently~\cite{Ham11}. Another notable model is \emph{term-rewriting} that is used with languages based on predicate logic~\cite{jaffar1987constraint}, such as Prolog~\cite{wielemaker2012swi}, where facts are represented by terms and new facts can be deduced from available facts. This model implements a variant of tree search and has proven particularly suitable for Artificial Intelligence applications. % ? Further models 

\vspace{-0.1cm}
\subparagraph*{OS Interaction} Commonly, an RTS relies on the OS for resource allocation, I/O and networking and provides a layer hiding OS-specific details and offering an \emph{architecture-independent} API to systems programmers. RTS often directly implements higher-level language features and lower-level language primitives that are difficult to implement efficiently in the language itself. Hiding low-level details from the programmers is beneficial for productivity by providing abstractions and by transparently handling boilerplate initialisation, clean-up and error checking. For instance, channels in Erlang~\cite{armstrong1996erlang} offer a higher level communication mechanism than sockets~\cite{silberschatz1998operating} and in some cases communication and synchronisation are \emph{fully implicit}, as in the distributed GUM RTS for parallel Haskell. 

\vspace{-0.3cm}
\subparagraph*{Error and Exception Handling} Error handling is often fairly crude such as a global \texttt{errno} variable that can be explicitly checked along with the error code returned by the function. More advanced language features include custom signal handlers and exception handling using the \texttt{try}/\texttt{catch} mechanism which is explicit and often results in complex control flow. Sophisticated RTSes can transparently handle errors at run time as demonstrated by Erlang's supervision mechanism, where failed processes can be restarted and resume execution.

\vspace{-0.3cm}
\subparagraph*{Memory Management} Most high-level languages require the RTS to support \emph{garbage collection} (GC) to automatically manage allocation and reclamation of heap objects. There are several common GC schemes~\cite{jones2011garbage}. One mechanism is \emph{reference-counting} where a counter indicates the number of references to an object which can be recovered once this count drops to zero. By contrast, \emph{mark-and-sweep} GC walks through the heap and marks all objects to be collected which is more disruptive but is able to collect reference cycles. Moreover, GC can be \emph{compacting} to avoid fragmentation by periodically grouping the used objects together, and \emph{generational} where objects reachable for a long time are promoted to a less often GC'd heap region, based on the insight that long surviving objects are likely to survive longer and most objects expire after a short period of time. A potential scalability issue on multicores is the use of a \emph{stop-the-world} GC which suspends all execution when GC is performed, suggesting the use of private heaps across cores to enable \emph{concurrent} or \emph{distributed} GC. 

\vspace{-0.3cm}
\subparagraph*{Thread and Task Management} OS-level threads and RTS-level tasks (or \emph{lightweight threads}) are at the heart of support for concurrency and parallelism, which is a key source of application performance on modern hardware. Often the RTS manages a thread pool and a task pool multiplexing tasks onto a set of OS threads for scalability. Many languages provide explicit threaded programming model, however, this model is deemed a poor choice due to observable non-determinism and notoriously difficult to detect and correct \emph{race conditions} and \emph{deadlocks}~\cite{lee2006problem}. Thus higher-level deterministic programming models such as Transactional Memory~\cite{herlihy1993transactional} and semi-explicit or skeleton-based models have gained increasing attention~\cite{HW-MACS-TR-0103}. 

Furthermore, explicit \emph{synchronisation} involves a granularity trade-off that is detrimental to portable performance: a global lock would be safe but sacrifices parallelism, whereas fine-grained locking may result in prohibitive overheads. Coordination of execution, including scheduling and work distribution across heterogeneous architectures substantially increases the complexity of RTS decisions. This is the reason why \emph{optional} tasks or threads are preferable to \emph{mandatory} ones: the RTS only executes a task in parallel if it appears worthwhile~\cite{THMP96,fluet2010implicitly}. Two main families of work distribution mechanisms are \emph{work pushing} (or work sharing) where work is offloaded eagerly and \emph{work stealing}~\cite{BlLe99}, a decentralised and adaptive mechanism where work distribution is demand-driven. 

\vspace{-0.3cm}
\subparagraph*{Communication}
Large-scale applications require scalability beyond a single node mandating distribution of work across a cluster, a Grid, or in the Cloud. Two common schemes include \emph{shared memory} (which can be distributed, requiring an additional abstraction layer) and \emph{message passing}. Serialisation of computation and data is required if communication occurs across a network. A low-level library such as MPI~\cite{gropp1999using} can be used to implement communication functionality. More sophisticated protocols such as publish/subscribe~\cite{eugster2003many} may be beneficial for distributed cooperative RTSes which share contextual information. % to improve decisions. 

\vspace{-0.3cm}
\subparagraph*{Monitoring}
Profiling, debugging, and execution replay rely on tracing (logging of relevant events), whereas dynamic optimisations rely on system information obtained through monitoring at run time. Common profiling strategies are, in order of increasing overhead, cumulative summary statistics, census-based statistics (collected at times of disruption, e.g. GC), fine-grained event-based statistics (using a sampling frequency or recording all events).

\section{A Qualitative Run-Time System Comparison}

Table~\ref{tab:rtss} provides an overview of several mainstream and research RTSes regarding their support for the features introduced above. We observe that a stack machine implementation is most common for mainstream languages whilst graph reduction is popular among declarative implementations. CLR provides support for tail call optimisation, so that declarative languages can be supported (e.g.\ F\#), whereas JVM lacks direct support. JVM and CLR are compared in more detail elsewhere~\cite{gough2001stacking,singer2003jvm}, and although superficially similar they have many distinctive features (e.g.\ unlike JVM, CLR was designed for multi-language support). 

\begin{table}[!ht]%[h!t!p!]
\caption{A High-Level Overview of Ten Representative Run-Time Systems}
\label{tab:rtss}
\vspace{-0.3cm}
\begin{center}
\begin{tabular}{llllll}

 \rowcolor{Gray} RTS/         & Exec.     & Concurrency/    & Memory       & Commun./       & Adaptive     \\ 
 \rowcolor{Gray} Language     & Model     & Parallelism     & Management   & Synchron.      & Policies     \\

 \hline
                \textbf{crt0} & stack     & libs: fork,     & explicit     &  sockets, MPI/ & user-        \\
                    C         & machine   &  Pthreads       & malloc/free  &  mutex, locks  & defined      \\

 \rowcolor{Gray} \textbf{JVM} & stack     & libraries       & impl.\ gen.  & sockets, RPC/  & JIT          \\
 \rowcolor{Gray} Java/Scala/..& machine   & Threads, ..      & mark\&sweep  & synchronized   &              \\

             \textbf{CLR}     & stack     & async/futures   & implicit     & sockets/     & JIT          \\
              C\#/F\#/..      & machine   & PLINQ, ..           & gen.\ m\&s   & locks, wait         &              \\

 \rowcolor{Gray} \textbf{GHC-SMP} & graph      & forkIO, STM, & implicit      & libraries/  & work         \\ 
 \rowcolor{Gray} Haskell          & reduction  & Par monad, ..     & gen. m\&s     & MVars,TVars & stealing     \\

                 \textbf{GUM}     & graph      & par, Eval    & impl.\ distr. & both implicit  & work         \\
                 Haskell ext.         & reduction  & Strategies   & ref.\ count.  & (graph nodes) & stealing   \\

 \rowcolor{Gray} \textbf{Manticore} & graph     & expl.\ tasks/  & impl.\ gen.\  & expl.\ mesg./    & lazy tree    \\
 \rowcolor{Gray} CML ext.           & reduction & impl.\ data  & local/global  & sync    & splitting    \\

 \textbf{X10 RTS}                   & stack     & async, futur./      & impl.\ distr. & PGAS/      & work         \\  %(JVM-based) async.\ PGAS
  X10                               & machine   & foreach, ..    & ref.\ count.  & atomic      & stealing     \\ %coop. schedul

 \rowcolor{Gray} \textbf{BEAM}     & register   & spawn        & per process   & expl.\ mesg./ & hot code     \\ % vs BEAM vs HiPE
 \rowcolor{Gray} Erlang            & machine    & primitive    & compact./gen. & mesg.\ boxes    & swapping     \\

 \textbf{Cilk}                     & stack      & spawn,       & impl.\ cactus  & explicit/  & work         \\ %shared-memory 
  C extension                      & machine    & cilk func.   & stack          & sync       & stealing     \\

 \rowcolor{Gray} \textbf{SWI Prolog}  & term    & threads/   & impl. low      & sockets/      & assert/      \\
 \rowcolor{Gray} ISO Prolog        & rewriting  & concurrent          & prio. thread   & mutex, join     & retract      \\ % unification

\end{tabular}
\end{center}
\label{table1}
\end{table}
\vspace{-0.3cm}

As concurrency and parallelism support grow more important we observe that such support was added as an afterthought and often as a non-composable library to the mainstream languages. By contrast, Erlang was designed for concurrency and natively supports channels which facilitate communication among lightweight processes. Additionally, declarative languages often are a better match for parallelism due to immutability and graph reduction execution model~\cite{Ham11}. For example, Haskell provides support for lightweight threads and data parallelism, whereas GUM supports adaptive distributed execution of GpH programs~\cite{marlow2009runtime,THMP96}.

Most of the RTSes support some kind of GC but only in few cases a more scalable design is used (e.g.\ Erlang uses per-process GC, X10~\cite{charles2005x10} and GUM use distributed GC). However, an empirical study is necessary to judge on the relative merits of different schemes.
In mainstream languages communication and synchronisation are often explicit, increasing the complexity of concurrent and parallel programming. From this angle GpH appears as a very-high-level language in which all such aspects are implicit, whereas communication in X10 is implicit using the \emph{Partitioned Global Address Space} (PGAS) abstraction whereas locality settings for the distributed data structures are explicitly provided by the programmer. 

Many of the RTSes also employ adaptive optimisations of some kind such as JIT, work-stealing, or self-adjusting granularity control to throttle parallelism depending on available resources (e.g.\ in GUM). Like memory management, this is an area of ongoing research~\cite{arnold2005survey}.

\section{Alternative Views on the Role of the Run-Time System}

The proliferation of Cloud Computing as an economical approach to elastically scaling IT infrastructure based on actual demand and of Big Data Analysis in addition to Scientific High-Performance Computing applications have lead to an increased interest in parallel and distributed RTSes capable of managing execution across distributed platforms at large scale. Additionally, cache coherence protocols exhibit limited scalability and many novel architectures (e.g.\ custom System-on-Chip architectures) resemble distributed systems to some extent and may be non-cache-coherent~\cite{peter2011early}. 

Moreover, as many programming languages were not designed with parallelism and distribution in mind, their RTSes provide only minimal support for rather low-level coordination. This is an issue since most programmers are not parallelism and distributed systems experts and explicitly specifying coordination is deemed prohibitively unproductive, favouring flexible and safe RTS approaches. Convergence in RTS and OS functionality can be observed in addition to the aforementioned architectural trends that lead to promising alternative views.


\vspace{-0.2cm} 
\subsection{Holistic Run-Time System}
 A Holistic RTS~\cite{maas2014case} would move from separate RTS instances running on top of a host OS to a distributed RTS which takes over most of the OS functionalities and provides support for multiple languages simultaneously. The RTS would manage multiple applications at the same time and in a distributed fashion to avoid interference and coordinate distributed GC across isolated heaps. Additional benefits arise from the ability to share libraries and RTS components as well as to better utilise higher-level information for holistic optimisation.

\vspace{-0.2cm}
\subsection{Application-Specific Run-Time System}
At the other end of the spectrum, an alternative approach is based on a generic distributed OS~\cite{peter2011early} (e.g.\ based on the \emph{Multikernel} approach~\cite{baumann2009multikernel}) offering a standardised interface for RTSes to communicate their demands, facilitating cooperative execution and helping avoid pathological interference. This way RTSes could be more lightweight and tailored for specific applications taking additional context information about application characteristics into account. To achieve this, a novel standard OS-RTS-interaction interface is required that would allow the OS to schedule RTSes and mediate the negotiations among them. 

\section{Challenges and Opportunities for Future Research}
Recent trends towards heterogeneous and hierarchical parallel architectures and increased demand for high-level
language features pose substantial challenges for the design and implementation of efficient language run-time systems offering opportunities for research.

\vspace{-0.2cm}
\subsection{Adaptation and Auto-Tuning for Performance Portability}
As argued above, manual program tuning is deemed infeasible for most application domains, thus rendering automatic
compiler and RTS optimisations critical for performance portability and scalability. Alas, many decisions have been shown to be NP-hard in general, thus requiring
heuristics and meta-heuristics to facilitate heuristics choice based on available context information.
Machine Learning has been shown to improve compiler flag selection~\cite{fursin2011milepost} and it appears that 
similar techniques may be applicable for RTS-level adaptation. To achieve performance portability, the RTS
must provide some performance guarantees based on applications' requirements and available architectural and system-level information. 

\vspace{-0.2cm}
\subsection{Fault Tolerance}
Traditionally, language RTS designs have focused on language features and performance, yet mechanisms to transparently detect, avoid, and recover from failures are becoming more important, as hardware failure is rather the norm than exception in modern large-scale systems~\cite{avivzienis2004basic}. In particular, the mean time between failures is rapidly decreasing following the exponential growth of the transistor budget in accordance with Moore's Law.

\vspace{-0.2cm}
\subsection{Safety and Security} 
One of the key benefits of using RTSes is that untrusted code can be executed safely in a sandboxed environment so that
other applications are safeguarded from many negative effects that could be caused by malicious code. Additionally, intermediate code can be verified and type-checked by the RTS (e.g.\ as done by JVM's bytecode verifier) to eliminate the possibility of whole classes of common errors. Furthermore, automatic GC eliminates the source of memory corruption errors and avoids the buffer overflow vulnerability. It is an open issue to ensure safe and secure execution of sensitive workloads on Cloud-based infrastructures, which is the reason why many companies are reluctant to use new technologies or prefer investing into private data centres. Support for dependent and session types is another research direction focused on improving the safety
of high-level languages~\cite{mckinna2006dependent,honda2008multiparty}.

\vspace{-0.2cm}
\subsection{Unified Distributed Run-Time Environment}
As described above, both alternative RTS approaches require tighter integration of RTS and OS functions favouring cross-layer information flow. Moreover, a unified and extensible RTS needs to support diverse execution models for multi-language interoperability, as has been demonstrated by the CLR. The holistic view would facilitate avoidance of interference and enable cooperative solutions for resource management. Ultimately, sophisticated RTSes would hide coordination and management complexities from programmers increasing productivity and avoiding explicit overspecification of coordination to achieve high performance portability across diverse target platforms. On the other hand, it may be worthwhile to spatially and temporally separate applications and grant exclusive access to some resources on massively parallel architectures~\cite{liu2009tessellation}. 


\section{Conclusion}
Based on the current trends, modern programming languages are likely to increasingly rely on a sophisticated RTS to manage efficient program execution across diverse target platforms. We have reviewed the role of the RTS in the software life cycle and  
the key RTS components and associated policies. Additionally, a qualitative comparison of mainstream and research RTSes illustrated the breadth of the design choices across the dimensions of execution models, memory management, support for concurrency and parallelism, as well as communication and adaptive policy control. The overview is complemented by references to influential historical results and current research outcomes alongside a discussion of alternative RTS designs followed by an outline of several promising areas for future research.

\vspace{-0.2cm}
\subparagraph*{Acknowledgements}
The author is grateful to Greg Michaelson and Hans-Wolfgang Loidl for thought-provoking discussions, to Julian Godesa, Konstantina Panagiotopoulou and Prabhat Totoo for useful comments, and to Siân Robinson Davies for designing the figures, as well as to the three anonymous reviewers for valuable feedback that improved this paper.

\vspace{-0.5cm}
\bibliography{main}

\end{document}